package com.puc.airbnb.repositories;


import com.puc.airbnb.entities.Imoveis;
import com.puc.airbnb.enums.ImoveisTipoEnum;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface ImoveisRepository extends MongoRepository<Imoveis, String> {

//      @Query("{'tipoImovel':?0,'disponivel':?1}")
//      List<Imoveis> findImoveisByTipoImovelAndDisponivel(ImoveisTipoEnum tipoEnum,Boolean disponivel);
//--------------------------------------------------------------------------------------------
//      @Query("{'tipoImovel': #{#tipoEnum}, 'disponivel': #{#disponivel}") utilizados
//      List<Imoveis> findImoveisByTipoImovelAndDisponivel(@Param("tipoEnum")  ImoveisTipoEnum tipoEnum,@Param("disponivel") Boolean disponivel);
      List<Imoveis> findImoveisByTipoImovelAndDisponivel(ImoveisTipoEnum tipoEnum,Boolean disponivel);


}
