package com.puc.airbnb.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.puc.airbnb.entities.Usuario;

public interface UsuarioRepository  extends MongoRepository<Usuario, String> {

    public Usuario findByUser(String user);
}
    
