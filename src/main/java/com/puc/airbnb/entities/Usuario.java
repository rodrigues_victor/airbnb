package com.puc.airbnb.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.puc.airbnb.enums.UsuarioTipoEnum;

@Document(collection = "usuarios")
public class Usuario {
    @Id
    private String id;
    private String user;
    private String senha;
    private UsuarioTipoEnum tipo;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public UsuarioTipoEnum getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = UsuarioTipoEnum.fromString(tipo);
    }

    public String getId() {
        return id;
    }

}
