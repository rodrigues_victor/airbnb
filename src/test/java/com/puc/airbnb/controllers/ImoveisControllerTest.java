package com.puc.airbnb.controllers;

import com.puc.airbnb.config.CsrfTokenGen;
import com.puc.airbnb.config.WithMockCustomUser;

import com.puc.airbnb.entities.Imoveis;
import com.puc.airbnb.services.ImoveisService;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.google.gson.Gson;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(ImoveisController.class)
// @AutoConfigureMockMvc(addFilters = false)
public class ImoveisControllerTest {

        @MockBean
        private ImoveisService imoveisService;

        @Autowired
        private MockMvc mockMvc;

        // var TOKEN_ATTR_NAME =
        // "org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository.CSRF_TOKEN";
        // var httpSessionCsrfTokenRepository = new HttpSessionCsrfTokenRepository();
        // var csrfToken = httpSessionCsrfTokenRepository.generateToken(new
        // MockHttpServletRequest());
        // usado criar informações do csrf token
        private CsrfTokenGen csrfTokenGen = new CsrfTokenGen();

    @Test
    @WithMockCustomUser
    public void deveListarTodosOsImoveis() throws Exception {
            List<Imoveis> imoveisLista = Arrays.asList(
                Imoveis.builder()
                        .valorDiaria(125.52)
                        .nomeImovel("Quarto menina")
                        .build(),
                Imoveis.builder().valorDiaria(200.32)
                        .nomeImovel("Apartamento luxo")
                        .build()
        );

        Mockito.when(imoveisService.listarTodos()).thenReturn(imoveisLista);

        mockMvc.perform(get("/imoveis")).andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].valorDiaria").value("125.52"))
                .andExpect(jsonPath("$.[0].nomeImovel").value("Quarto menina"))
                .andExpect(jsonPath("$.[1].valorDiaria").value("200.32"))
                .andExpect(jsonPath("$.[1].nomeImovel").value("Apartamento luxo"));

        Mockito.verify(imoveisService).listarTodos();

    }


    @Test
    @WithMockCustomUser(roles = "locador")
    public void deveCadartrarImoveis() throws Exception {
        Imoveis imovel= Imoveis.builder()
                .valorDiaria(125.52)
                .nomeImovel("Quarto menina")
                .build();
        Mockito.when(imoveisService.inserir(imovel)).thenReturn(imovel);

        var json= new Gson().toJson(imovel);

        mockMvc.perform(post("/imoveis/inserir")
                .sessionAttr(csrfTokenGen.getSessionNomeToken(), csrfTokenGen.getSessionCsrfToken())
                .param(csrfTokenGen.getParamNome(), csrfTokenGen.getParamToken())
                // as duas linhas de cima são para passar o token csrf, tanto no atributo da sessão quanto no parametro
                // vão ser utilizadas em todo teste do tipo post, put, delete
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json))
        .andExpect(MockMvcResultMatchers.status().isCreated())
        .andExpect(jsonPath("$.valorDiaria").value("125.52"))
        .andExpect(jsonPath("$.nomeImovel").value("Quarto menina"));

    }

}
