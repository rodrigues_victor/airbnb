package com.puc.airbnb.config;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import com.puc.airbnb.entities.Usuario;
import com.puc.airbnb.security.UserPrincipal;

public class WithMockCustomUserSecurityContextFactory implements
        WithSecurityContextFactory<WithMockCustomUser> {

    @Override
    public SecurityContext createSecurityContext(WithMockCustomUser annotation) {
        SecurityContext context = SecurityContextHolder.createEmptyContext();

        Usuario usuario = new Usuario();
        usuario.setUser(annotation.user());
        usuario.setTipo(annotation.roles());
        usuario.setSenha("password");

        UserPrincipal principal = UserPrincipal.create(usuario);
        Authentication auth = UsernamePasswordAuthenticationToken.authenticated(principal, "password",
                principal.getAuthorities());

        context.setAuthentication(auth);

        return context;
    }

}
