package com.puc.airbnb.config;

import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

public class CsrfTokenGen {
    // usado para criar informações do csrf token

    //
    private final String TOKEN_ATTR_NAME = "org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository.CSRF_TOKEN";
    private final HttpSessionCsrfTokenRepository httpSessionCsrfTokenRepository = new HttpSessionCsrfTokenRepository();
    private final CsrfToken csrfToken = httpSessionCsrfTokenRepository.generateToken(new MockHttpServletRequest());

    public String getParamToken() {
        return csrfToken.getToken(); // valor do token
    }

    public String getParamNome() {
        return csrfToken.getParameterName(); // nome do token
    }

    public String getSessionNomeToken() {
        return TOKEN_ATTR_NAME; // nome token sessão
    }

    public CsrfToken getSessionCsrfToken() {
        return csrfToken; // token sessão
    }

}
